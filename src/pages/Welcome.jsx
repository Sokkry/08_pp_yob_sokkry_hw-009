import React from 'react'
import { Button } from 'react-bootstrap'
export default function Welcome(props) {
   
    return (
        <div>
            <h1>Welcome</h1>
                <a href="/auth">
                    <Button variant="warning" onClick={props.OnSignin}> Logout</Button>
                </a>
        </div>
    )
}
