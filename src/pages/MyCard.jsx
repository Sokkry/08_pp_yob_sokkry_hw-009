
import React, { Component } from 'react'
import { Button, Card, Row, Col } from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default class MyCard extends Component {

    constructor(props) {
        super(props)

        this.state = {
            Data: [
                {
                    id: 1,
                    title: "Look at me",
                    content: "I not cute",
                    image: "/images/cat01.jpg"
                },
                {
                    id: 2,
                    title: "I am so speepy",
                    content: "I don't know about lession",
                    image: "/images/cat02.jpg"
                },
                {
                    id: 3,
                    title: "Two Brothers",
                    content: "I is so happy.",
                    image: "/images/cat03.jpg"
                },
                {
                    id: 4,
                    title: "I am a cat",
                    content: "Don't call me dog.",
                    image: "/images/cat04.jpg"
                },
                {
                    id: 5,
                    title: "Small family",
                    content: "With Mak Mak & Pa Pa ",
                    image: "/images/cat05.jpg"
                },
                {
                    id: 6,
                    title: "I am so cute",
                    content: "Fail in love",
                    image: "/images/cat06.jpg"
                },
            ]
        }
    }


    render() {
        return (
            <div>
                <Row>
                    {
                        this.state.Data.map((item, index) => (
                            <Col lg={3} key={index}>
                                <Card style={{ width: '18rem' }}>
                                    <Card.Img variant="top" src={item.image} />
                                    <Card.Body>
                                        <Card.Title>{item.title} </Card.Title>
                                        <Card.Text>{item.content}  </Card.Text>
                                        <Link as={Link} to={`/detail/${item.id}`} >
                                            <Button variant="primary">Read</Button>
                                        </Link>
                                    </Card.Body>
                                </Card>
                            </Col>
                        ))
                    }
                </Row>
            </div>
        )
    }
}


