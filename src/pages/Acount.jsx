import React from 'react'
import { Link, Route, Switch, useParams } from 'react-router-dom'

export default function Acount() {
    return (
        <div>
            <h1>Account</h1>
            <ul>
                <li><Link to="/acount/netflix">Netflix</Link></li>
                <li><Link to="/acount/zillow-group">Zillow Group</Link></li>
                <li><Link to="/acount/yahoo">Yahoo</Link></li>
                <li><Link to="/acount/mods-create">Mods Create</Link></li>
            </ul>
            <Switch>
                <Route path="/acount/:name" component={InAccount} />
            </Switch>
            
        </div>
    )
}

function InAccount(){
    let param = useParams();
    return(
        <h2>ID: {param.name} </h2>
    )
}
