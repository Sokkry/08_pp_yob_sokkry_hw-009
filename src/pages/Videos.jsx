import React from 'react'
// import { Nav } from 'react-bootstrap'
import { BrowserRouter as Router, Switch, Route, Link, useLocation } from "react-router-dom";
import queryString from 'query-string'
import { Button } from 'react-bootstrap'
export default function Videos() {

    return (
        <div>
            <h1>Videos</h1>
            <Router>
                <div>
                    <Link to="/movie">
                        <Button variant="primary">Movies</Button>
                    </Link>

                    <Link to="/animation">
                        <Button variant="primary">Animation</Button>
                    </Link>
                    <Switch>
                        <Route exact path="/movie">
                            <Movie />
                        </Route>
                        <Route path="/animation">
                            <Animation />
                        </Route>
                    </Switch>
                </div>
            </Router>
        </div>
    )
}

function Movie() {
    return (
        <div>
            <h2> Movies Category</h2>
            <Link to="/movie?type=Adventure">
                <Button variant="primary">Adventure</Button>
            </Link>
            <Link to="/movie?type=Crime">
                <Button variant="primary">Crime</Button>
            </Link>
            <Link to="/movie?type=Action">
                <Button variant="primary">Action</Button>
            </Link>
            <Link to="/movie?type=Romance">
                <Button variant="primary">Romance</Button>
            </Link>
            <Link to="/movie?type=Comedy">
                <Button variant="primary">Comedy</Button>
            </Link>

            <Switch>
                <Route path="/movie">
                    <InMovie />
                </Route>
            </Switch>
        </div>

    );
}
function InMovie() {
    const location = useLocation()
    const query = queryString.parse(location.search)
    return (
        <div>
            <h3> Please Choose Category :  <span className="ColRed">{query.type}</span></h3>
        </div>
    );
}
// ========
function Animation() {
    return (
        <div>
            <h2> Animation Category</h2>
            <Link to="/Animation?type=Action">
                <Button variant="primary">Action</Button>
            </Link>
            <Link to="/Animation?type=Romance">
                <Button variant="primary">Romance</Button>
            </Link>
            <Link to="/Animation?type=Comedy">
                <Button variant="primary">Comedy</Button>
            </Link>

            <Switch>
                <Route path="/Animation">
                    <InAnimation />
                </Route>
            </Switch>
        </div>

    );
}
function InAnimation() {
    const location = useLocation()
    const query = queryString.parse(location.search)
    return (
        <div>
            <h3> Please Choose Category :  <span className="ColRed">{query.type}</span></h3>
        </div>
    );
}


