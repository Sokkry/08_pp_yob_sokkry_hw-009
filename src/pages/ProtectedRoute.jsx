import React from 'react'
import { Redirect } from 'react-router'

export default function ProtectedRoute({IsSignin, component: Component, path }) {
    if(IsSignin){
        return <Component path={path} />
    }else{
        return <Redirect to="/auth" />
    }
}
