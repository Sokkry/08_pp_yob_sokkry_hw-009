import React from 'react'
import { useParams } from 'react-router'

export default function CardDetail() {
    let param = useParams();
    return (
        <div>
            <h2>Detail Card Number : {param.id}  </h2>
        </div>
    )
}
