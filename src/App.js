
import './App.css';
import React, {useState} from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'react-bootstrap';
import Menu from './components/Menu';
import Auth from './pages/Auth';
import Acount from './pages/Acount';
import MyCard from './pages/MyCard';
import Videos from './pages/Videos';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Welcome from './pages/Welcome';
import CardDetail from './pages/CardDetail';
import ProtectedRoute from './pages/ProtectedRoute';

function App() {
  const [IsSignin, setIsSignin] = useState(false)
  function OnSignin() {
    setIsSignin(!IsSignin)
  }
  return (
    <Container>
      <Router>
        <Menu />
        <Switch>
          <Route exact path="/" component={MyCard} />
          <Route path="/video" component={Videos} />
          <Route path="/acount" component={Acount} />

          {/* <Route path="/welcome" component={Welcome} IsSignin={IsSignin}/> */}

          <ProtectedRoute IsSignin={IsSignin} component={Welcome} path="/welcome" />

          {/* <Route path="/auth" component={Auth} OnSignin={OnSignin}  /> */}
          <Route path="/auth"  render={()=><Auth OnSignin={OnSignin} IsSignin={IsSignin} />} />
          <Route path="/detail/:id" component={CardDetail} />
        </Switch>
      </Router>
    </Container>
  );
}

export default App;
